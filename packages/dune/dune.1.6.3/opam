opam-version: "2.0"
maintainer: "opensource@janestreet.com"
authors: ["Jane Street Group, LLC <opensource@janestreet.com>"]
homepage: "https://github.com/ocaml/dune"
bug-reports: "https://github.com/ocaml/dune/issues"
dev-repo: "git+https://github.com/ocaml/dune.git"
license: "MIT"
depends: [
  "ocaml" {>= "4.02"}
  "base-unix"
  "base-threads"
]
build: [
  # opam 2 sets OPAM_SWITCH_PREFIX, so we don't need a hardcoded path
  ["ocaml" "configure.ml" "--libdir" lib] {opam-version < "2"}
  ["ocaml" "bootstrap.ml"]
  ["./boot.exe" "--release" "--subst"] {pinned}
  ["./boot.exe" "--release" "-j" jobs]
]
conflicts: [
  "jbuilder" {!= "transition"}
  "odoc" {< "1.3.0"}
]

synopsis: "Fast, portable and opinionated build system"
description: """
dune is a build system that was designed to simplify the release of
Jane Street packages. It reads metadata from "dune" files following a
very simple s-expression syntax.

dune is fast, it has very low-overhead and support parallel builds on
all platforms. It has no system dependencies, all you need to build
dune and packages using dune is OCaml. You don't need or make or bash
as long as the packages themselves don't use bash explicitly.

dune supports multi-package development by simply dropping multiple
repositories into the same directory.

It also supports multi-context builds, such as building against
several opam roots/switches simultaneously. This helps maintaining
packages across several versions of OCaml and gives cross-compilation
for free.
"""
url {
  src: "https://github.com/ocaml/dune/releases/download/1.6.3/dune-1.6.3.tbz"
  checksum: [
    "md5=1212a36547d25269675d767c38fecf5f"
    "sha256=bfd52160d88578c404af1267abfba6eb4928988e51aef0e92dbebdea1607ae36"
    "sha512=29eb1abf2236150bc6dbb64be09ca84a72b8ae5f0742623497ca7958bf15652b507592f51e934cf94172583e773beeab0b205ff4e5480f42ce28a2c256803b65"
  ]
}
